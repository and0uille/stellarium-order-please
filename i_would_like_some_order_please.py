#!/usr/bin/env python

import logging
import argparse

catalog='/usr/share/stellarium/stars/default/stars_0_0v0_8.cat'
catalog_custom='/tmp/stars_0_0v0_8-custom.cat'

def read_header(catalog):
    header = {}

    catalog.seek(0)
    magicbytes = catalog.read(4)
    catalog.seek(4)
    header["datatype"] = int.from_bytes(catalog.read(4), "little")
    catalog.seek(8)
    header["majorversion"] = catalog.read(4)
    catalog.seek(12)
    header["minorversion"] = catalog.read(4)
    catalog.seek(16)
    # sets the level of sub-division of the geodesic sphere used to create the
    # zones. 
    # - 0 means an icosahedron (20 triangular faces), 
    # - subsequent levels of sub-division lead to other number of zones 
    header["level"] = int.from_bytes(catalog.read(4), "little")
    catalog.seek(20)
    header["magnitudemin"] = int.from_bytes(catalog.read(4), "little")
    catalog.seek(24)
    header["magnituderange"] = catalog.read(4)
    catalog.seek(28)
    header["magnitudestep"] = catalog.read(4)

    logging.info("Data type: " + str(header["datatype"]))
    logging.info("Major version: " + str(int.from_bytes(header["majorversion"], "little")))
    logging.info("Minor version: " + str(int.from_bytes(header["minorversion"], "little")))
    logging.info("Level: " + str(header["level"]))
    logging.info("Magnitude min: " + str(header["magnitudemin"]))
    logging.info("Magnitude range: " + str(int.from_bytes(header["magnituderange"], "little")))
    logging.info("Magnitude step: " + str(int.from_bytes(header["magnitudestep"], "little")))
    return header


def read_record_lvl0(catalog):
    record_start = 32
    record_length = 25
    catalog.seek(24)
    header["magnituderange"] = catalog.read(4)


if __name__ == "__main__":
    
    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--debug', dest='debug', default=False, 
        action='store_true', help='Activer le mode debug (debug.log)')
    args = parser.parse_args()

    if args.debug == True:
        logging.basicConfig(filename='./debug.log',level=logging.DEBUG)

    # Parse star catalog
    with open(catalog, 'rb') as catalog:
        with open(catalog_custom, 'ab+') as catalog_custom:
            header = read_header(catalog)

            # Copy header from original to custom file
            catalog.seek(0)
            catalog_custom.seek(0)
            catalog_custom.write(catalog.read(32))

            # Number of star records there are per zone. The number of zones is
            # determined from the level value in the File Header Record. The
            # Zones section is simply a list of integer values which describe
            # the number of stars for each zone. The total length of the Zones
            # section depends on the number of zones.
            num_of_zones = 20*4**header["level"]
            logging.info("Number of zones: "+str(num_of_zones))

            # Number of star in each zone
            zone_record = []
            for i in range(0, num_of_zones): 
                catalog.seek(32+i*4)
                catalog_custom.seek(32+i*4)
                catalog_custom.write(catalog.read(4))
                zone_record.append(int.from_bytes(catalog.read(4), "little"))
            logging.info("Zone record (num of stars / zone): "+str(zone_record))
            logging.info("Total number of stars: " + str(sum(zone_record)))

            
            # Parsing star records
            if header["datatype"] == 0:
                logging.info("Datatype 0")
                star_record_size = 28

                star_ctr = 0
                for zone in range(0, num_of_zones):
                    for star_num in range(0, zone_record[zone]):
                        # Star record starts after: 
                        # - file header
                        # - Zone record section
                        # - Previous star records
                        star_offset = 32+num_of_zones*4+star_record_size*star_ctr

                        catalog.seek(star_offset)
                        catalog_custom.seek(star_offset)
                        hip = catalog.read(3)
                        catalog_custom.write(hip)

                        catalog.seek(star_offset+3)
                        component_ids = catalog.read(1)
                        catalog_custom.write(component_ids)

                        catalog.seek(star_offset+3+1)
                        x0 = catalog.read(4)
                        catalog_custom.write(b'\xE2\x9E\x2B\xD7')

                        catalog.seek(star_offset+3+1+4)
                        x1 = catalog.read(4)
                        catalog_custom.write(x1)

                        catalog.seek(star_offset+3+1+4+4)
                        bv = catalog.read(1)
                        catalog_custom.write(bv)

                        catalog.seek(star_offset+3+1+4+4+1)
                        mag = catalog.read(1)
                        catalog_custom.write(mag)

                        catalog.seek(star_offset+3+1+4+4+1+1)
                        sp_int = catalog.read(2)
                        catalog_custom.write(sp_int)

                        catalog.seek(star_offset+3+1+4+4+1+1+2)
                        dx0 = catalog.read(4)
                        catalog_custom.write(dx0)

                        catalog.seek(star_offset+3+1+4+4+1+1+2+4)
                        dx1 = catalog.read(4)
                        catalog_custom.write(dx1)

                        catalog.seek(star_offset+3+1+4+4+1+1+2+4+4)
                        plx = catalog.read(4)
                        catalog_custom.write(plx)

                        logging.info("Offset: " + str(star_offset))
                        logging.info("Zone: " + str(zone+1)+"/"+str(num_of_zones))
                        logging.info("HIP: " + str(int.from_bytes(hip, "little")))
                        logging.info(" - x0: " + str(int.from_bytes(x0, "little")))
                        logging.info(" - x1: " + str(int.from_bytes(x1, "little")))
                        logging.info(" - bv: " + str(int.from_bytes(bv, "little")))
                        logging.info(" - mag: " + str(int.from_bytes(mag, "little")))

                        star_ctr = star_ctr+1

