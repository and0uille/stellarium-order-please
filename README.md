# Stellarium order please

## Description 

This is the dirty code written to make a video meme on Stellarium. It copies 
the star catalog file `stars_0_0v0_8.cat` and recreates a custom one with the 
`x0` parameter of each star record set to a random value.

![Stellarium-order](sample.png)

## Warning

Make sure that you have a backup of `stars_0_0v0_8.cat` before you overwrite it.

## Contact

Feel free to fork or reach out to me on Mastodon
([@and0uille@mastodon.zaclys.com](https://mastodon.zaclys.com/web/@and0uille))
or Twitter ([@and0uille](https://twitter.com/and0uille)).

## Documentation

Useful documentation can be found in Stellarium User guide (Appendix C). 
